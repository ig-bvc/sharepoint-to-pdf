import os
import xml.etree.ElementTree as ET
import re
import html
from re import Pattern
from typing import Tuple
import csv


def main():
    u_path = os.getenv("U_PATH", "ueberschriften.xml")
    u_tree = ET.parse(u_path)
    u_root = u_tree.getroot()

    xpath = "./feed/entry[1]/content/m:properties/d:LinkFilename"
    xpath = ".//{http://schemas.microsoft.com/ado/2007/08/dataservices}LinkFilename"
    bsi = u_root.findall(xpath)
    
    with open("titles.csv", "w", newline='', encoding="utf-8") as title_file:
        titleWriter = csv.writer(title_file, delimiter=',',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for e in bsi:
            titleWriter.writerow([e.text[:-5] if e.text.endswith('.aspx') else e.text])


if __name__ == '__main__':
    main()
