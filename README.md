# PDF der Ausarbeitungen

Python Script, welches alle Ausarbeitungen der IG BvC aus dem Sharepoint (https://portale-dvz.mvnet.de/dvz/standdatenzentralen/) einliest und eine möglichst druckfähige HTML Version dieser erzeugt.

## Download der neusten PDF builds
- [IG BvC APP.4.4](https://gitlab.opencode.de/api/v4/projects/40/jobs/artifacts/main/raw/igbvc-app-4-4.pdf?job=generate-pdf)
- [IG BvC SYS.1.6](https://gitlab.opencode.de/api/v4/projects/40/jobs/artifacts/main/raw/igbvc-sys-1-6.pdf?job=generate-pdf)

## Sonstiges
Builds finden täglich um 04:00 Uhr morgens statt. Gegenüber dem IG SharePoint wird der User `PORTALE-DVZ\funktionsbenutzer` genutzt. 
