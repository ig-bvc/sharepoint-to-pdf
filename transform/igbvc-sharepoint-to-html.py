import os
import xml.etree.ElementTree as ET
import re
import html
from re import Pattern
from typing import Tuple


def main():
    a_path = os.getenv("A_PATH", "anforderungen.xml")
    u_path = os.getenv("U_PATH", "ueberschriften.xml")
    a_tree = ET.parse(a_path)
    u_tree = ET.parse(u_path)
    a_root = a_tree.getroot()
    u_root = u_tree.getroot()
    ns = {'atom': 'http://www.w3.org/2005/Atom'}
    titles = get_titles(u_root, ns)
    requirement_texts = get_requirement_texts(a_root, ns)

    pattern = re.compile(r'SYS.1.6.A(\d+)')
    sys_1_6_html = reduce_to_html_by_titles(requirement_texts, titles, pattern)
    with open("igbvc-sys-1-6.html", "w") as outfile:
        outfile.write(sys_1_6_html)

    pattern = re.compile(r'APP.4.4.A(\d+)')
    app_4_4_html = reduce_to_html_by_titles(requirement_texts, titles, pattern)
    with open("igbvc-app-4-4.html", "w") as outfile:
        outfile.write(app_4_4_html)


def reduce_to_html_by_titles(requirement_texts, titles, pattern) -> str:
    titles, requirement_texts = filter_by_title_containing(titles, requirement_texts, pattern)
    titles = sort_by(titles, pattern)
    return generate_html(titles, requirement_texts)


def get_titles(root: ET.Element, ns: dict) -> dict:
    titles = {}
    for title_element in root.findall("atom:entry", ns):
        title_text = title_element.find('atom:content', ns)[0][0].text
        if title_text.endswith('.aspx'):
            title_text = title_text[:-5]
        id_text = title_element.find("atom:id", ns).text
        sharepoint_list_id = int(re.findall(r'\d+', id_text)[-1])
        titles[sharepoint_list_id] = title_text
    return titles


def get_requirement_texts(root: ET.Element, ns) -> dict:
    requirement_texts = {}
    for requirement_element in root.findall("atom:entry", ns):
        id_text = requirement_element.find("atom:id", ns).text
        sharepoint_list_id = int(re.findall(r'\d+', id_text)[-1])

        for content in requirement_element.iter('{http://schemas.microsoft.com/ado/2007/08/dataservices}WikiField'):
            requirement_texts[sharepoint_list_id] = content.text
    return requirement_texts


def filter_by_title_containing(titles, requirement_texts, pattern: Pattern) -> Tuple[dict, dict]:
    # remove unrelated entries (anything not containing pattern in title)
    titles = {sharepoint_list_id: title for sharepoint_list_id, title in titles.items() if pattern.search(title)}
    requirement_texts = {sharepoint_list_id: title_text for sharepoint_list_id, title_text in requirement_texts.items() if sharepoint_list_id in titles}
    return titles, requirement_texts


def sort_by(titles, pattern: Pattern) -> dict:
    return dict(
        sorted(
            titles.items(),
            key=lambda item: int(pattern.search(item[1]).group(1))
        )
    )


def generate_html(titles_sorted_by_bsi_id, requirement_texts) -> str:
    with open(f"{os.path.dirname(os.path.realpath(__file__))}/igbvc-style.css", "r") as styles_css:
        styles = styles_css.read()

    out_html = f"""
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>IG BvC - Anforderungen</title>
        <style>
        {styles}
        </style>
    </head>
    <body>
    <h1>IG Betrieb von Containern</h1>
    """

    content_html = ""
    table_of_contents_html = '<h2>Inhaltsverzeichnis</h2>\n<ul id="table-of-contents">\n'
    for sharepoint_list_id, title in titles_sorted_by_bsi_id.items():
        content = html.unescape(requirement_texts[sharepoint_list_id]).replace('\u200b', '<wbr>')
        table_of_contents_html += f'  <li><a href="#{sharepoint_list_id}">{title}</a></li>'
        content_html += f'<h1 id="{sharepoint_list_id}" class="title">{title}</h1>' + '\n'
        content_html += f'<p>{content}</p><hr>'
        content_html += '\n'
    table_of_contents_html += '</ul>'

    out_html += table_of_contents_html
    out_html += content_html
    out_html += '<div class="igbvc-footer">Vertraulich - Arbeitsentwurf - IG Betrieb von Containern - https://portale-dvz.mvnet.de/dvz/standdatenzentralen</div>'
    out_html += '</body>\n<html>'

    return out_html


if __name__ == '__main__':
    main()
