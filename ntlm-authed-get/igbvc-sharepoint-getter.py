import requests
import os
from requests_ntlm2 import HttpNtlmAuth

username = os.getenv('OC_USERNAME')
password = os.getenv('OC_PASSWORD')
for param in [username, password]:
    if(not (param and not param.isspace())):
        print(f"ERROR - {param} is empty")
        exit(1)

list_guid = os.getenv('SP_LIST_GUID', "c77cc4ca-b1f5-438b-b3ff-c4b8a68689d5")

auth = HttpNtlmAuth(username, password)

url = f"https://portale-dvz.mvnet.de/dvz/standdatenzentralen/_api/Web/Lists(guid'{list_guid}')/items"

content = requests.get(url, auth=auth)
titles = requests.get(f"{url}?$select=LinkFilename", auth=auth)

with open("content.xml", "w", encoding="utf-8") as content_file:
    # Writing data to a file
    content_file.write(content.text)

with open("titles.xml", "w", encoding="utf-8") as titles_file:
    # Writing data to a file
    titles_file.write(titles.text)
